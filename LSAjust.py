import math

from numpy import *

from LSMatrices import *

class LSAjust:
    def __init__(self, matrices):
        self.matrices = matrices
        self.ComputeAjus ()

        self.Compute_Df_rm_Sigma()

    def ComputeAjus (self):
        
        self.matrices.N = self.matrices.A.T * self.matrices.W * self.matrices.A

        # least squares solution
        self.matrices.DX = self.matrices.N.I * self.matrices.A.T * self.matrices.W * self.matrices.DY

        # residuals computation
        self.matrices.V = self.matrices.A * self.matrices.DX - self.matrices.DY

#        print()
#        print('deslocamentos (DX)')
#        print(self.matrices.DX)

    # compute reference standard deviation
    def Compute_Df_rm_Sigma (self):
        n = len(self.matrices.DX)
        m = len(self.matrices.DY)

        self.matrices.df = m - n

        self.matrices.r_m = self.matrices.df / m

        vtWv = self.matrices.V.T * self.matrices.W * self.matrices.V

        self.matrices.sigma = math.sqrt( vtWv[0,0] / self.matrices.df )
