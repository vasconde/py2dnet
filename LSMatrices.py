
# data structers
import Points
import Obs

# derivatives (observation equations)
import DeriObs

# ordinary math functions
import math

# linear algebra
from numpy import *


class LSMatrices:                                    # pus 'm' para na acontecer nada
    def __init__(self, data, ang_units = 'decimiligon', lin_units = 'm'):
        
        self.data = data
        
        self.obs = self.data.obs
        self.pts = self.data.points
        
        self.conf_ang = self.SetConf_ang (ang_units)
        self.conf_lin = self.SetConf_lin (lin_units)
        
        self.X = [] # vector of parameters
        self.DY = [] # vector of dif of observations
        self.W = [] # weights matrix (pt. MCSO)
        self.A = [] # design matrix (pt. MCPO)

        self.npoints = 0    # number of points
        self.nobs = 0       # number of observations
        self.nobs_ang = 0
        self.nobs_dist = 0
        self.nobs_quo = 0
        self.n_cond_eq = 0

        # load apriori matrices
        self.MatApriori ()

        # aposteriori
        self.N = [] # (A^T W A) matrix
        self.DX = [] # vector of displacements
        self.V = [] # vector of residuals

        self.df = 0 # degrees of freedom # calculated with sigma
        self.r_m = 0 # redundancy mean # calculated with sigma
        self.sigma = None # Reference standard deviation (aposteriori)

    # update displacements of points
    def UpdatePoints (self):
        
        i = 0
        for p in self.pts.unknowns:
            p.dx = self.DX[i,0]
            i += 1
            p.dy = self.DX[i,0]
            i += 1
        for p in self.pts.controls:
            p.dx = self.DX[i,0]
            i += 1
            p.dy = self.DX[i,0]
            i += 1

    # update displacements of points
    def UpdateObs (self):
        
        i = 0
        
        # ang
        for ba in self.obs.basisAngs:
            for a in ba.angs:
                a.residual = self.V[i,0]
                i += 1

        # dist
        for d in self.obs.dists:
            d.residual = self.V[i,0]
            i += 1

        # quo
        for q in self.obs.quos:
            q.residual = self.V[i,0]
            i += 1

    # update attributes of data (pos ajustment)
    def UpdateData (self):
        
        self.data.npoints = self.npoints
        
        self.data.nobs = self.nobs
        self.data.nobs_ang = self.nobs_ang
        self.data.nobs_dist = self.nobs_dist
        self.data.nobs_quo = self.nobs_quo
        self.data.n_cond_eq = self.n_cond_eq

        self.data.sigma = self.sigma
        self.data.df = self.df
        self.data.r_m = self.r_m

        # update points
        self.UpdatePoints ()

        # update Obs
        self.UpdateObs ()

        

    # coefficient for convert angular units
    # used in design matrix
    def SetConf_ang (self, ang_units):

        if ang_units == 'rad':
            return 1.0
        elif ang_units == 'gon':
            return 200.0/math.pi
        elif ang_units == 'miligon':
            return 200000.0/math.pi
        elif ang_units == 'decimiligon':
            return 2000000.0/math.pi

    # coefficient for convert linear units
    # lin_units to meters
    def SetConf_lin (self, lin_units):

        if lin_units == 'm':
            return 1.0
        elif lin_units == 'mm':
            return 1.0/1000.0

    # generate matrix of approximate coordinates
    def GApproximate_coordinates_X (self):

        for p in self.pts.unknowns:
            self.X.append( [p.x * self.conf_lin] )
            self.X.append( [p.y * self.conf_lin] )
        for p in self.pts.controls:
            self.X.append( [p.x * self.conf_lin] )
            self.X.append( [p.y * self.conf_lin] )

        self.X = matrix(self.X)

        self.npoints = len(self.X) / 2 # update the number os points

    # given a point name return index in X (approximate coordinates)
    def GetIAcoordinates (self, name):
        i = 0
        for p in self.pts.unknowns:
            if (p.name == name):
                return i
            i += 2

        for p in self.pts.controls:
            if (p.name == name):
                return i
            i += 2

        return None

    # given a point name return coordinates in X (approximate coordinates)
    def GetAcoordinates (self, name):
        i = self.GetIAcoordinates (name)
        
        if ( i != None ):
            return [ self.X[i,0], self.X[i+1,0] ]
        else:
            return None

    ## compute euclidian distance between a and b
    ## computes euclidean distance between a and b
    def Calc_dist_2 (self,a,b):
        return (b[0] - a[0])**2 + (b[1] - a[1])**2 
    def Calc_dist (self,a,b):
        return math.sqrt( self.Calc_dist_2 (a,b) )

    # WARNING : angular units are gon
    # WARNING : linear units are ln(d_dist)
    def GDifObs_DY (self):
        
        self.nobs = 0       # number of observations
        self.nobs_ang = 0
        self.nobs_dist = 0
        self.nobs_quo = 0
        self.n_cond_eq = 0
        
        # ang
        for ba in self.obs.basisAngs:
            for a in ba.angs:
                self.DY.append([ a.obs ])
                self.nobs_ang += 1

        # dist
        for d in self.obs.dists:
            self.DY.append([ d.obs ])
            self.nobs_dist += 1

        '''
        # dist (ln(d1)- ln(d2))
        for d in self.obs.dists:  

            self.DY.append([ d.obs / self.Calc_dist (self.GetAcoordinates(d.occupied),
                                                     self.GetAcoordinates(d.sighted)) ])
                                                     '''

        # quo
        for q in self.obs.quos:
            ratio = (self.Calc_dist(self.GetAcoordinates(q.occupied),self.GetAcoordinates(q.sighted))
                     / 
                     self.Calc_dist(self.GetAcoordinates(q.occupied),self.GetAcoordinates(q.origin)))

            self.DY.append([ q.obs / ratio ])
            self.nobs_quo += 1

        # control points
        for p in self.pts.controls:
            if p.who == 'XY':
                self.DY.append([ p.dx_a * self.conf_lin ])
                self.DY.append([ p.dy_a * self.conf_lin ])
                self.n_cond_eq += 2
            elif p.who == 'X':
                self.DY.append([ p.dx_a * self.conf_lin ])
                self.n_cond_eq += 1
            elif p.who == 'Y':
                self.DY.append([ p.dy_a * self.conf_lin ])
                self.n_cond_eq += 1
            
        
        self.DY = matrix(self.DY)

        self.nobs = self.nobs_ang + self.nobs_dist + self.nobs_quo

    def GSubWeightsAng_Wang (self, basis):
        n = len(basis.angs)
        SW = matrix(ones( [n,n] ) + identity(n))

        SW = (1.0 / ( 2.0 * (basis.stdev) **2)) * SW.I

        SW = matrix(SW)

        # update the stdev apriori in data
        i = 0
        for a in basis.angs:
            a.stdev_a = math.sqrt( 1.0 / SW[i,i] )
            i += 1
        
        return SW

        

    # WARNING : angular units are gon
    # WARNING : linear units are ln(d_dist)
    def GWeights_W (self):
        self.W = matrix(zeros([size(self.DY),size(self.DY)]))

        i=0
        # ang
        for ba in self.obs.basisAngs:
            
            SW = self.GSubWeightsAng_Wang (ba)
            
            self.W[i:i+SW.shape[0],i:i+SW.shape[1]] = SW

            i += SW.shape[0]
            
        # dist
        for d in self.obs.dists:
            self.W[i,i] = 1.0 / ( 2*(d.e_mm + d.e_ppm * 1e-6)**2 )

            # update the stdev apriori in data
            d.stdev_a = math.sqrt( 1.0 / self.W[i,i] )

            i += 1

    
####        # dist (ln) *   stdev^2 = a^2 + b^2 * s^2    *
####        for d in self.obs.dists:
####            self.W[i,i] = 1.0 / ( 2.0 * d.stdev**2 / 
####                                  ( self.Calc_dist (self.GetAcoordinates(d.occupied),
####                                                    self.GetAcoordinates(d.sighted)))**2 )
####            i += 1

        # quo
        for q in self.obs.quos:
            self.W[i,i] =  1.0 / ( 2.0 * q.stdev**2 /
                                   (self.Calc_dist (self.GetAcoordinates(q.occupied),
                                                    self.GetAcoordinates(q.sighted))
                                    /
                                    self.Calc_dist (self.GetAcoordinates(q.occupied),
                                                    self.GetAcoordinates(q.origin)))**2 )
            i += 1

        # control points
        for p in self.pts.controls:
            if p.who == 'XY':
                self.W[i,i] =  1.0 / (p.stdev_dx_a * self.conf_lin)**2 
                i += 1
                self.W[i,i] =  1.0 / (p.stdev_dy_a * self.conf_lin)**2 
                i += 1
            elif p.who == 'X':
                self.W[i,i] =  1.0 / (p.stdev_dx_a * self.conf_lin)**2 
                i += 1
            elif p.who == 'Y':
                self.W[i,i] =  1.0 / (p.stdev_dy_a * self.conf_lin)**2 
                i += 1
            

        self.W = matrix(self.W)

    def GDesign_A (self):

        self.A = zeros([size(self.DY),size(self.X)])

        i = 0

        for ba in self.obs.basisAngs:
            for a in ba.angs:
                # get coordinates
                origin = self.GetAcoordinates (a.origin)
                occupied = self.GetAcoordinates (a.occupied)
                sighted = self.GetAcoordinates (a.sighted)
            
                # compute derivatives
                derivatives = DeriObs.d_ang (origin,occupied,sighted)

                # get points idexes in vector X
                iorigin = self.GetIAcoordinates (a.origin)
                ioccupied = self.GetIAcoordinates (a.occupied)
                isighted = self.GetIAcoordinates (a.sighted)

                # fills matrix A with derivatives
                self.A[i,iorigin] = derivatives[0][0] * self.conf_ang
                self.A[i,iorigin+1] = derivatives[0][1] * self.conf_ang

                self.A[i,ioccupied] = derivatives[1][0] * self.conf_ang
                self.A[i,ioccupied+1] = derivatives[1][1] * self.conf_ang

                self.A[i,isighted] = derivatives[2][0] * self.conf_ang
                self.A[i,isighted+1] = derivatives[2][1] * self.conf_ang
                
#                print(10 * '-')
#                print(i,a.origin,a.occupied,a.sighted)
#                print(10 * '-')
#
#                print(self.A[i,iorigin])
#                print(self.A[i,iorigin+1])
#
#                print(self.A[i,ioccupied])
#                print(self.A[i,ioccupied+1])
#
#                print(self.A[i,isighted])
#                print(self.A[i,isighted+1])

                

                i += 1 # next line of matrix A

        # dist (ln)
        for d in self.obs.dists:
            # get coordinates
            occupied = self.GetAcoordinates (d.occupied)
            sighted = self.GetAcoordinates (d.sighted)

            # compute derivatives
            derivatives = DeriObs.d_dist (occupied,sighted)

            # get points idexes in vector X
            ioccupied = self.GetIAcoordinates (d.occupied)
            isighted = self.GetIAcoordinates (d.sighted)

            # fills matrix A with derivatives
            self.A[i,ioccupied] = derivatives[0][0]
            self.A[i,ioccupied+1] = derivatives[0][1]

            self.A[i,isighted] = derivatives[1][0]
            self.A[i,isighted+1] = derivatives[1][1]

            i += 1 # next line of matrix A


        
####        # dist (ln)
####        for d in self.obs.dists:
####
####            # get coordinates
####            occupied = self.GetAcoordinates (d.occupied)
####            sighted = self.GetAcoordinates (d.sighted)
####
####            # compute derivatives
####            derivatives = DeriObs.d_ln_dist (occupied,sighted)
####
####            # get points idexes in vector X
####            ioccupied = self.GetIAcoordinates (d.occupied)
####            isighted = self.GetIAcoordinates (d.sighted)
####
####            # fills matrix A with derivatives
####            self.A[i,ioccupied] = derivatives[0][0]
####            self.A[i,ioccupied+1] = derivatives[0][1]
####
####            self.A[i,isighted] = derivatives[1][0]
####            self.A[i,isighted+1] = derivatives[1][1]
####
####            i += 1 # next line of matrix A
           

        # quo
        for q in self.obs.quos:
            # get coordinates
            origin = self.GetAcoordinates (q.origin)
            occupied = self.GetAcoordinates (q.occupied)
            sighted = self.GetAcoordinates (q.sighted)
            
            # compute derivatives
            derivatives = DeriObs.d_ln_quo_dist (origin,occupied,sighted)

            # get points idexes in vector X
            iorigin = self.GetIAcoordinates (q.origin)
            ioccupied = self.GetIAcoordinates (q.occupied)
            isighted = self.GetIAcoordinates (q.sighted)

            # fills matrix A with derivatives
            self.A[i,iorigin] = derivatives[0][0]
            self.A[i,iorigin+1] = derivatives[0][1]

            self.A[i,ioccupied] = derivatives[1][0]
            self.A[i,ioccupied+1] = derivatives[1][1]

            self.A[i,isighted] = derivatives[2][0]
            self.A[i,isighted+1] = derivatives[2][1]

            i += 1 # next line of matrix A

        # control points
        
        for p in self.pts.controls:
            if p.who == 'XY':
                self.A[i, self.GetIAcoordinates (p.name)] =  1.0
                i += 1
                self.A[i, self.GetIAcoordinates (p.name) + 1] =  1.0
                i += 1
            elif p.who == 'X':
                self.W[i, self.GetIAcoordinates (p.name)] =  1.0
                i += 1
            elif p.who == 'Y':
                self.W[i, self.GetIAcoordinates (p.name) + 1] =  1.0
                i += 1
                
        self.A = matrix(self.A)

    def MatApriori (self):
        self.GApproximate_coordinates_X ()
        self.GDifObs_DY ()
        self.GWeights_W ()
        self.GDesign_A ()

    def MatAposteriori (self):
        pass
