import Points
import Obs
import Data

import ReadTxtF

import LSMatrices
import LSAjust

import WriteTxtF

import math

o = Obs.Obs()
p = Points.Points()
d = Data.Data (p, o)



ReadTxtF.ReadTxtF(d, "./data/obs.txt", "./data/coo.txt", "./data/fixos.txt")


m = LSMatrices.LSMatrices(d)
#m = LSMatrices.LSMatrices(o,p,'rad')


a = LSAjust.LSAjust(m)

m.UpdateData () # after ajustment and before write results

w = WriteTxtF.WriteTxtF(d, m, a, "./data/results.txt", "./data/des.txt")




w.WriteResultsPT ()
