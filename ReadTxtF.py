
import Points
import Obs

class ReadTxtF:
    def __init__(self, data, obs_txt = 'obs.txt', 
                 coo_txt = 'coo.txt', control_txt = 'fixos.txt'):
        # objects

        self.data = data

        self.points = self.data.points
        self.obs = self.data.obs

        # files
        self.obs_txt = obs_txt
        self.coo_txt = coo_txt
        self.control_txt = control_txt

        # load data from files to objects
        self.Txt2obs ()
        self.Txt2points ()

    # load from text file to Points object
    def Txt2points (self):

        # first of all extract the lines from file to a list
        lines = self.Txt2lines_list (self.control_txt)

        control_temp = [line.rsplit('|') for line in lines]


        # first of all extract the lines from file to a list
        lines = self.Txt2lines_list (self.coo_txt)

        pts = [line.rsplit('|') for line in lines]

        for pt in pts:
            if (pt[0] in [ctr[0] for ctr in control_temp]): # if it is control

                ic = [ctr[0] for ctr in control_temp].index(pt[0]) # control index

                self.points.Add_point(Points.Point(pt[0], float(pt[1]), float(pt[2]), True,
                                                   control_temp[ic][1], 
                                                   float(control_temp[ic][2]),
                                                   float(control_temp[ic][3]),
                                                   float(control_temp[ic][4]), 
                                                   float(control_temp[ic][5]) ))
            else:
                self.points.Add_point(Points.Point(pt[0], float(pt[1]), float(pt[2])))
        

    # load from text file to Obs object
    def Txt2obs (self):
        # first of all extract the lines from file to a list
        lines = self.Txt2lines_list (self.obs_txt)

        # index of each group of observations
        # WARNING : if more than one group of the same type
        groups_ix = []

        i = 0
        for line in lines:
            if (line[0:3] == 'ANG'):
                groups_ix.append([i,'ANG'])
            elif (line[0:4] == 'DIST'):
                groups_ix.append([i,'DIST'])
            elif (line[0:3] == 'QUO'):
                groups_ix.append([i,'QUO'])
            i += 1
                
        # sort by index
        groups_ix.sort()

        # split into groups (lists of groups)
        groups = []
        i = 0
        for t in groups_ix:
            if (i < len(groups_ix)-1):
                groups.append( lines[groups_ix[i][0]:groups_ix[i+1][0]] )
            else:
                groups.append( lines[groups_ix[i][0]:] )
            i = i + 1

        # groups to data structers
        for g in groups:
            if (g[0][0:3] == 'ANG'):
                
                # create a new basis of angles
                b = Obs.BasisAng( [] , float(g[0].rsplit('|')[1]) )
                
                # fill the basis with angles
                for el in [o.rsplit('|') for o in g[1:]]:
                    b.Add_ang( Obs.Ang(el[0], el[1], el[2], float(el[3])) )

                # add basis to the main struct of observations data
                self.obs.Add_basisAng(b)

            elif (g[0][0:4] == 'DIST'):

                e_mm = float(g[0].rsplit('|')[1])
                e_ppm = float(g[0].rsplit('|')[2])

                for el in [o.rsplit('|') for o in g[1:]]:
                    self.obs.Add_dist( Obs.Dist(el[0], el[1], float(el[2]), 0, e_mm, e_ppm) )

            elif (g[0][0:3] == 'QUO'):
                for el in [o.rsplit('|') for o in g[1:]]:
                    self.obs.Add_quo( Obs.Quo(el[0], el[1], el[2], float(el[3]), float(el[4])) )
        
            # update de Stdev apriori
            self.obs.UpdateAngStdev_a ()
            self.obs.UpdateDistStdev_a ()

    # generic function that receive a file name and return a list with
    # the lines in this file (string format)
    def Txt2lines_list (self, file_path):
    
        # read line by line and put it in a list
        obs_file = open(file_path, 'rt')
        lines = [line.strip() for line in obs_file] #list of strings
        obs_file.close()
        
        # remove comments and empty lines
        lines_without_comments = []
        for line in lines:
            if line != '':
                if line[0] != '#':
                    lines_without_comments.append(line)

        return lines_without_comments
