
import Points
import Obs

class Data:
    def __init__ (self, points, obs):

        # Before ajustment

        self.points = points
        self.obs = obs

        # number of points
        self.npoints = 0

        # number of observations
        self.nobs = 0

        self.nobs_ang = 0
        self.nobs_dist = 0
        self.nobs_quo = 0
        self.n_cond_eq = 0

        # After ajustment

        # degrees of freedom
        self.df = 0

        # redundancy mean
        self.r_m = 0

        # Reference standard deviation (aposteriori)
        self.sigma = 0

        
        
