
import Points
import Obs

# for writing time in text file
from time import gmtime, strftime

class WriteTxtF:
    def __init__(self, data, m, a, res_file_path = 'res.txt', des_file_path = ''):
        
        self.data = data

        self.points = self.data.points
        self.obs = self.data.obs

        self.m = m # matrices
        self.a = a # ajustment

        # results of ajustment text file
        self.res_file_path = res_file_path

        # description text file
        self.des_file_path = des_file_path

    def SetPoints (self, points):
        self.points = points

    def SetObs (self, obs):
        self.obs = obs
        

    def WriteResultsPT (self):

        res_file = open(self.res_file_path, 'wt')

        # HEAD

        res_file.write(80*'*')
        res_file.write('\n')
        res_file.write( '{:^80}'.format('PY2DNET') )
        res_file.write('\n')
        res_file.write( '{:^80}'.format('Versão: Beta1') )
        res_file.write('\n')
        res_file.write('{:^80}'.format(strftime('%Y-%m-%d %H:%M:%S', gmtime())))
        res_file.write('\n')
        res_file.write(80*'*')
        res_file.write('\n')

        if self.des_file_path != '':
            res_file.write( '{:^80}'.format('Descrição') )
            res_file.write('\n')

            # read line by line and put it in a list
            des_file = open(self.des_file_path, 'rt')
            lines = [line.strip() for line in des_file] #list of strings
            des_file.close()

            for line in lines:
                if line[0] != '#':
                    res_file.write(line)
                    res_file.write('\n')

            res_file.write(80*'*')
            res_file.write('\n')
            
        # RESULTS
        res_file.write( '{:^80}'.format('Resultados') )
        res_file.write('\n')

        res_file.write('\n')

        res_file.write('1) Informação')
        res_file.write('\n')
        res_file.write('\n')
        
        res_file.write(' - Numero de pontos ................ {0}'.format(self.data.npoints))
        res_file.write('\n')
        res_file.write('\n')
        res_file.write(' - Numero de observações ........... {0}'.format(self.data.nobs))
        res_file.write('\n')
        res_file.write('   - Ângulos ....................... {0}'.format(self.data.nobs_ang))
        res_file.write('\n')
        res_file.write('   - Distâncias .................... {0}'.format(self.data.nobs_dist))
        res_file.write('\n')
        res_file.write('   - Quocientes .................... {0}'.format(self.data.nobs_quo))
        res_file.write('\n')
        res_file.write(' - Numero de eq. de condição ....... {0}'.format(self.data.n_cond_eq))
        res_file.write('\n')

        res_file.write('\n')
        res_file.write(' - Numero de graus de liberdade .... {0}'.format(self.data.df))
        res_file.write('\n')
        res_file.write(' - Redundância média ............... {0}'.format(self.data.r_m))
        res_file.write('\n')

        res_file.write('\n')
        res_file.write('2) Desvio padrão da unidade de peso (a posteriori)')
        res_file.write('\n')
        res_file.write('\n')
        res_file.write(' - Sigma ........................... {0}'.format(self.data.sigma))
        res_file.write('\n')

        res_file.write('\n')
        res_file.write('3) Deslocamentos estimados')
        res_file.write('\n')
        res_file.write('\n')
        
        res_file.write(' {0:10} {1:>10} {2:>10}'.format('Ponto', 'DX', 'DY'))
        res_file.write('\n')
        res_file.write(' ' + 32*'-')
        res_file.write('\n')
        
        for u in self.points.unknowns:
            res_file.write(' {0:10} {1:10.1f} {2:10.1f}'.format(u.name, u.dx, u.dy))
            res_file.write('\n')
        
        for c in self.points.controls:
            res_file.write(' {0:10} {1:10.1f} {2:10.1f}'.format(c.name, c.dx, c.dy))
            res_file.write('\n')

        res_file.write('\n')
        res_file.write('4) Observações e resíduos')
        res_file.write('\n')
        res_file.write('\n')

        
        if len(self.obs.basisAngs) != 0:
            res_file.write('\n')
            res_file.write('-- Ângulos --')
            res_file.write('\n')
            res_file.write('\n')

            res_file.write('{0:10}{1:10}{2:10}  {3:>10}{4:>10}{5:>10}{6:>10}'.format('Ref.', 'Est.', 'Vis.', 'Obs.', 'Res.', 'Obs. Comp.', 'DP priori'))
            res_file.write('\n')
            res_file.write(72*'-')
            res_file.write('\n')


        basis_gt_1 = False
        for ba in self.obs.basisAngs:

            if basis_gt_1:
                res_file.write('*\n')

            for ob in ba.angs:
                o_comp = ob.obs + ob.residual
                res_file.write('{0:10}{1:10}{2:10}  {3:10.1f}{4:10.1f}{5:10.1f}{6:10.1f}'.format(ob.origin, ob.occupied,ob.sighted, ob.obs, ob.residual, o_comp, ob.stdev_a))
                res_file.write('\n')

            basis_gt_1 = True

        if len(self.obs.dists) != 0:
            res_file.write('\n')
            res_file.write('-- Distâncias --')
            res_file.write('\n')
            res_file.write('\n')

            res_file.write('{0:10}{1:10}  {2:>10}{3:>10}{4:>10}{5:>10}'.format('Est.', 'Vis.', 'Obs.', 'Res.', 'Obs. Comp.', 'DP priori'))
            res_file.write('\n')
            res_file.write(62*'-')
            res_file.write('\n')

        for ob in self.obs.dists:
            o_comp = ob.obs + ob.residual
            res_file.write('{0:10}{1:10}  {2:10.1f}{3:10.1f}{4:10.1f}{5:10.1f} '.format(ob.occupied,
                                                                                        ob.sighted,
                                                                                        ob.obs,
                                                                                        ob.residual,
                                                                                        o_comp,
                                                                                        ob.stdev_a))
            res_file.write('\n')

        '''
        res_file.write('\n')
        res_file.write('-- Pontos Fixos --')
        res_file.write('\n')
        res_file.write('\n')

        for po in self.points.controls:
            print('{0:10}  {1:10.1f}  {2:10.1f}'.format(po.name, self.m.V[i,0], po.stdev_dx_a))
            i += 1
            print('{0:10}  {1:10.1f}  {2:10.1f}'.format(po.name,self.m.V[i,0],po.stdev_dy_a))
            i += 1
        '''
        # FOOT
        res_file.write('\n')
        res_file.write(80*'*')
        res_file.write('\n')

        # close file
        res_file.close()

