
class Point:
    def __init__(self, name, x, y, 
                 control = False, 
                 who = '', dx = None, stdev_dx = None, dy = None, stdev_dy = None):

        self.name = name
        
        # approximate coordinates
        self.x = x
        self.y = y
        
        # displacement
        self.dx = 0
        self.dy = 0
        
        # is control or is not
        self.control = control
        
        # if control == True
        self.who = who
        self.dx_a = dx
        self.stdev_dx_a = stdev_dx
        self.dy_a = dy
        self.stdev_dy_a = stdev_dy
        


class Points:
    def __init__(self, controls = [], unknowns = []):
        self.controls = controls
        self.unknowns = unknowns

    def Add_point(self,p):
        if (p.control): # if it is a control point
            if ( not( p.name in [ el.name for el in self.controls ] ) ): # if not exist in list
                self.controls.append(p)
        else: # if it is a control point
            if ( not( p.name in [ el.name for el in self.unknowns ] ) ): # if not exist in list
                self.unknowns.append(p)
