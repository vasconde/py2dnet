# des:    script para ajustamento de uma triangulacao
#         sao determinadas as coordenadas a partir
#         das obeservações angulares
# author: vasconde
# versao: v1 13/01/07

import math
from numpy import *

from deri import d_ang

# --- Dados --- #

## Caminhos para os ficheiros de entrada

# observações distancias
nome_ang_file = "./data1_2/angs.txt"

# coordenadas aproximadas dos pontos da rede
nome_aprox_file = "./data1_2/coo.txt"

# pontos fixos
nome_fixos_file = "./data1_2/fixos.txt"

# --- Prog- --- #

## Programa principal

## recolha das observacoes

# recolhe as linhas do ficheiro (origem,estacao,visado,ang)
in_file = open(nome_ang_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [origem,estacao,visado,ang]
ang_str = [linha.rsplit('|') for linha in linhas] #lista de triplos (em listas)
#print(dist_str)

## recolha dos pontos fixos

# recolhe as linhas do ficheiro (ponto,XY)
in_file = open(nome_fixos_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [ponto,XY]
fixos = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
print('fixos')
print(fixos)
print()

## recolha de coordenadas aproximadas

# recolhe as linhas do ficheiro (ponto,X,Y)
in_file = open(nome_aprox_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [ponto,X,Y]
aprox_str = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
#print()
#print(aprox_str)

## coverter strings para double

aproxs = [[ap[0], float(ap[1]), float(ap[2])] for ap in aprox_str]
angs = [[el[0], el[1], el[2], float(el[3])] for el in ang_str]

print('coordenadas aproximadas')
print(aproxs)
print()
print('angulos observadas')
print(angs)
print()

## indices dos fixos
index_f = []
i = 0
for ap in aproxs:
    for f in fixos:
        if(ap[0] == f[0]):
            if(f[1] == 'XY'):
                index_f.append(i*2)
                index_f.append(i*2 + 1)
            elif(f[1] == 'X'):
                index_f.append(i*2)
            else:
                index_f.append(i*2 + 1)
    i=i+1

print('indices dos fixos')
print(index_f)
print()



## Cria vetor de parametros

X = []
for aprox in aproxs:
    X.append([aprox[1]])
    X.append([aprox[2]])
X = matrix(X)

print('parametros (X)')
print(X)
print()



## Criar vetor de observacoes

L = [ [ a[3] * math.pi/200 ] for a in angs ] # dist observadas

for i in index_f:     # coordenadas dos pontos fixos
    L.append([X[i]])

L = matrix(L)

print('observacoes (L)')
print(L)


## Criar matriz de pesos (unitaria) MCSO

W = identity(size(L))

i = -1
for ind in index_f:     # coordenadas dos pontos fixos
    W[i,i] = 100000000
    i = i - 1

print()
print('pesos (W)')
print(W)


i_ciclo = 0
cont = True
while cont:

## Cria matriz de configuracao de primeira ordem MCPO
## E matriz de fecho (omega)

    A = zeros([size(L),size(X)])

    omega = [] # fecho

    i = 0
    for an in angs:
        # saca os idices dos dos pontos
        io = [a[0] for a in aproxs].index(an[0])
        ie = [a[0] for a in aproxs].index(an[1])
        iv = [a[0] for a in aproxs].index(an[2])
    
        # saca as coordenadas dos pontos
        o = [float(X[io*2]),float(X[io*2+1])]
        e = [float(X[ie*2]),float(X[ie*2+1])]
        v = [float(X[iv*2]),float(X[iv*2+1])]

        deri = d_ang (o,e,v)

        A[i,io*2] = deri[0][0]
        A[i,io*2+1] = deri[0][1]

        A[i,ie*2] = deri[1][0]
        A[i,ie*2+1] = deri[1][1]

        A[i,iv*2] = deri[2][0]
        A[i,iv*2+1] = deri[2][1]


        omega.append([ math.atan2( v[0]-e[0] , v[1]-e[1] ) - math.atan2( o[0]-e[0] , o[1]-e[1] ) ]) # fecho

        i = i+1
    
    for ind in index_f:
        A[i,ind] = 1

        omega.append(X[ind]) # fecho

        i = i+1

    omega = matrix(omega) # fecho
#    omega = omega - L     # fecho
    omega = L - omega

#    print()
#    print('MCPO (A)')
#    print(A)
    
#    print()
#    print('omega')
#    print(omega)

    A = matrix(A)

    W = matrix(W)

    N = A.T * W * A

    delta = N.I*A.T*W*omega

#    print('delta')
#    print(delta)

    X = X + delta

    ## controlo do ciclo com base na convergencia das correcoes dos parametros

    control = 0
    for i in range(size(X)):
        if(not(i in index_f)):
            control += delta[i]**2
    control = math.sqrt(control)

#    print('control')
#    print(control)
    
    if(control <= 0.0001):
        cont = False
    else:
        cont = True

    i_ciclo = i_ciclo + 1


print()
print('numero de ciclos')
print(i_ciclo)

print()
print('parametros ajustados (X)')
print(X)

v = A * delta + omega

print()
print('residuos (v)')
print(v)

sM = v.T * W * v

df = size(L) - size(X)

print()
print('df')
print(df)

s = math.sqrt(float(sM)/df)

print()
print('s')
print(s)
