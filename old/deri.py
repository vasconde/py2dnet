# des: derivadas para formulação das equações de observação
# aut: vasconde

import math

# Derivada da Distancia entre a e b
# entra: ponto a [xa,ya] e ponto b [xb,yb]
# sai: [d/dx_a, d/dy_a] [d/dx_b, d/dy_b]
def d_dist (a,b):
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    d_ab = math.sqrt(dx**2 + dy**2)

    d_dxa = -dx/d_ab
    d_dya = -dy/d_ab

    d_dxb = dx/d_ab
    d_dyb = dy/d_ab

    return [[d_dxa,d_dya],[d_dxb,d_dyb]]

# Derivada do Logaritmo da Distancia entre a e b
# entra: ponto a [xa,ya] e ponto b [xb,yb]
# sai: [d/dx_a, d/dy_a] [d/dx_b, d/dy_b]
def d_ln_dist (a,b):
    dx = b[0] - a[0]
    dy = b[1] - a[1]
    d_ab = math.sqrt(dx**2 + dy**2)

    d_dxa = -dx/(d_ab**2)
    d_dya = -dy/(d_ab**2)

    d_dxb = dx/(d_ab**2)
    d_dyb = dy/(d_ab**2)

    return [[d_dxa,d_dya],[d_dxb,d_dyb]]

# Derivada do logaritmo do quaociente das distancias eo e ev
# entra: ponto o [xo,yo], ponto e [xe,ye], ponto v [xv,yv]
# sai: [d/dx_o, d/dy_o] [d/dx_e, d/dy_e] [d/dx_v, d/dy_v] 
def d_ln_quo_dist (o,e,v):

    dln_deo = d_ln_dist (e,o)
    dln_dev = d_ln_dist (e,v)

    # derivadas
    d_dxo = - dln_deo[1][0]
    d_dyo = - dln_deo[1][1]

    d_dxv = dln_dev[1][0]
    d_dyv = dln_dev[1][1]

    d_dxe = dln_dev[0][0] - dln_deo[0][0]
    d_dye = dln_dev[0][1] - dln_deo[0][1]

    # retorno
    return [[d_dxo,d_dyo],[d_dxe,d_dye],[d_dxv,d_dyv]]

# Derivada do angulo OEV (origem - estacao - visado)
# entra: ponto o [xo,yo], ponto e [xe,ye], ponto v [xv,yv]
# sai: [d/dx_o, d/dy_o] [d/dx_e, d/dy_e] [d/dx_v, d/dy_v] 
def d_ang (o,e,v):
    # estacao - origem
    dx_eo = o[0] - e[0]
    dy_eo = o[1] - e[1]
    d_eo_2 = dx_eo**2 + dy_eo**2

    # estacao - visado
    dx_ev = v[0] - e[0]
    dy_ev = v[1] - e[1]
    d_ev_2 = dx_ev**2 + dy_ev**2

    # derivadas
    d_dxo = - dy_eo / d_eo_2
    d_dyo =   dx_eo / d_eo_2

    d_dxv =   dy_ev / d_ev_2
    d_dyv = - dx_ev / d_ev_2

    d_dxe = - d_dxo - d_dxv
    d_dye = - d_dyv - d_dyo

    # retorno
    return [[d_dxo,d_dyo],[d_dxe,d_dye],[d_dxv,d_dyv]]
