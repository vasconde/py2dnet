
def read_file (file_path):
    
    # passa as linhas do ficheiro para uma lista
    obs_file = open(file_path, "rt")
    linhas = [line.strip() for line in obs_file] #lista de strings
    obs_file.close()

    return linhas

def read_obs (linhas):
    # saca os indices de cada grupo de observacoes
    # ATENCAO: Se houver mais que um grupo do mesmo tipo
    grupos = []
    if ("ANG" in linhas):
        iang = linhas.index("ANG")
        grupos.append([iang,"ANG"])
    if ("DIST" in linhas):
        idist = linhas.index("DIST")
        grupos.append([idist,"DIST"])
    if ("QUO" in linhas):
        iquo = linhas.index("QUO")
        grupos.append([iquo,"QUO"])
        
    # ordena segundo a ordem em linhas (indice)
    grupos.sort()

    # separa em grupos (listas de grupos)
    obs_aux = []
    i = 0
    for t in grupos:
        if (i < len(grupos)-1):
            obs_aux.append( linhas[grupos[i][0]:grupos[i+1][0]] )
        else:
            obs_aux.append( linhas[grupos[i][0]:] )
        i = i + 1
    
    # separa em listas de listas de listas de observaces [[tipo1, [[obs1, obs2, ...],[]] ],[tipo2, []], ... ]
    obs = []
    for o_a in obs_aux:
        if(o_a[0] == "ANG"):
            angs = [o.rsplit('|') for o in o_a[1:]] #lista de triplos (em listas)
            angs = [[el[0], el[1], el[2], float(el[3])] for el in angs]
            obs.append([o_a[0],angs])
        elif(o_a[0] == "DIST"):
            dists = [o.rsplit('|') for o in o_a[1:]] #lista de triplos (em listas)
            dists = [[el[0], el[1], float(el[2])] for el in dists]
            obs.append([o_a[0],dists])
        else:
            quos = [o.rsplit('|') for o in o_a[1:]] #lista de triplos (em listas)
            quos = [[el[0], el[1], el[2], float(el[3])] for el in quos]
            obs.append([o_a[0],quos])

    return obs


def read_aproxs (linhas):
    # passa as linhas para listas de pares [ponto,X,Y]
    aproxs_str = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
    aproxs = [[ap[0], float(ap[1]), float(ap[2])] for ap in aproxs_str]
    return aproxs

def read_fixos (linhas):
    fixos = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
    return fixos

#####################

linhas_obs = read_file("./data_todos/obs.txt")
linhas_aproxs = read_file("./data_todos/coo.txt")
linhas_fixos = read_file("./data_todos/fixos.txt")

obs = read_obs (linhas_obs)
aproxs = read_aproxs (linhas_aproxs)
fixos = read_fixos (linhas_fixos)


print (obs)

