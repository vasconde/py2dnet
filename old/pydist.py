# des:    script para ajustamento de uma trilateracao
#         sao determinadas as coordenadas a partir
#         das obeservações de distância
# author: vasconde
# versao: v1 12/12/14

import math
from numpy import *

from deri import d_dist

# --- Dados --- #

## Caminhos para os ficheiros de entrada

# observações distancias
nome_dist_file = "./data/dist.txt"

# coordenadas aproximadas dos pontos da rede
nome_aprox_file = "./data/coo.txt"

# pontos fixos
nome_fixos_file = "./data/fixos.txt"

# --- Prog- --- #

## Programa principal

## recolha das observacoes

# recolhe as linhas do ficheiro (estacao,visado,dist)
in_file = open(nome_dist_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [estacao,visado,distancia]
dist_str = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
#print(dist_str)

## recolha dos pontos fixos

# recolhe as linhas do ficheiro (estacao,visado,dist)
in_file = open(nome_fixos_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [estacao,visado,distancia]
fixos = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
print('fixos')
print(fixos)
print()

## recolha de coordenadas aproximadas

# recolhe as linhas do ficheiro (estacao,visado,dist)
in_file = open(nome_aprox_file, "rt")
linhas = [line.strip() for line in in_file] #lista de strings
in_file.close()
#print (pares_str)

# passa as linhas para listas de pares [estacao,visado,distancia]
aprox_str = [linha.rsplit('|') for linha in linhas] #lista de pares (em listas)
#print()
#print(aprox_str)

## coverter strings para double

aproxs = [[ap[0], float(ap[1]), float(ap[2])] for ap in aprox_str]
dists = [[el[0], el[1], float(el[2])] for el in dist_str]

print('coordenadas aproximadas')
print(aproxs)
print()
print('distancias observadas')
print(dists)
print()

## indices dos fixos
index_f = []
i = 0
for ap in aproxs:
    for f in fixos:
        if(ap[0] == f[0]):
            if(f[1] == 'XY'):
                index_f.append(i*2)
                index_f.append(i*2 + 1)
            elif(f[1] == 'X'):
                index_f.append(i*2)
            else:
                index_f.append(i*2 + 1)
    i=i+1

print('indices dos fixos')
print(index_f)
print()

## Cria vetor de parametros

X = []
for aprox in aproxs:
    X.append([aprox[1]])
    X.append([aprox[2]])
X = matrix(X)

print('parametros (X)')
print(X)
print()

## Criar vetor de observacoes

L = [ [d[2]] for d in dists ] # dist observadas

for i in index_f:     # coordenadas dos pontos fixos
    L.append([X[i]])

L = matrix(L)

print('observacoes (L)')
print(L)

## Criar matriz de pesos (unitaria) MCSO

W = identity(size(L))

i = -1
for ind in index_f:     # coordenadas dos pontos fixos
    W[i,i] = 100000000
    i = i - 1

print()
print('pesos (W)')
print(W)

i_ciclo = 0
cont = True
while cont:

## Cria matriz de configuracao de primeira ordem MCPO
## E matriz de fecho (omega)

    A = zeros([size(L),size(X)])

    omega = [] # fecho

    i = 0
    for dist in dists:
        # saca os idices dos dos pontos
        ia = [a[0] for a in aproxs].index(dist[0])
        ib = [a[0] for a in aproxs].index(dist[1])
    
        # saca as coordenadas dos pontos
        a = [float(X[ia*2]),float(X[ia*2+1])]
        b = [float(X[ib*2]),float(X[ib*2+1])]

        deri = d_dist (a,b)

        A[i,ia*2] = deri[0][0]
        A[i,ia*2+1] = deri[0][1]
        A[i,ib*2] = deri[1][0]
        A[i,ib*2+1] = deri[1][1]

        omega.append([sqrt( (b[0]-a[0])**2 + (b[1]-a[1])**2 )]) # fecho

        i = i+1
    
    for ind in index_f:
        A[i,ind] = 1

        omega.append(X[ind]) # fecho

        i = i+1

    omega = matrix(omega) # fecho
#    omega = omega - L     # fecho
    omega = L - omega

#    print()
#    print('MCPO (A)')
#    print(A)
    
#    print()
#    print('omega')
#    print(omega)

    A = matrix(A)

    W = matrix(W)

    N = A.T * W * A

    delta = N.I*A.T*W*omega

#    print('delta')
#    print(delta)

    X = X + delta

    ## controlo do ciclo com base na convergencia das correcoes dos parametros

    control = 0
    for i in range(size(X)):
        if(not(i in index_f)):
            control += delta[i]**2
    control = math.sqrt(control)

#    print('control')
#    print(control)
    
    if(control <= 0.0001):
        cont = False
    else:
        cont = True

    i_ciclo = i_ciclo + 1


print()
print('numero de ciclos')
print(i_ciclo)

print()
print('parametros ajustados (X)')
print(X)

v = A * delta + omega

print()
print('residuos (v)')
print(v)

sM = v.T * W * v

df = size(L) - size(X)

print()
print('df')
print(df)

s = math.sqrt(float(sM)/df)

print()
print('s')
print(s)
