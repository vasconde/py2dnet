
import math

# future improvements:
#    - validate data (e.g the origin is the same for all angs in a basisang)

class Ang:
    def __init__(self, origin, occupied, sighted, obs = 0.0, stdev_a = 0.0):
        self.origin = origin
        self.occupied = occupied
        self.sighted = sighted
        self.obs = obs
        
        self.stdev_a = stdev_a

        self.residual = 0

class BasisAng:
    def __init__(self, angs = [], stdev = 0.0):
        self.angs = angs
        self.stdev = stdev
    
    def Add_ang(self,o):
        self.angs.append(o)

class Dist:
    def __init__(self, occupied, sighted, obs = 0.0, stdev_a = 0.0, e_mm = 1, e_ppm = 0):

        self.occupied = occupied
        self.sighted = sighted
        self.obs = obs

        self.e_mm =  e_mm
        self.e_ppm =  e_ppm

        self.stdev_a = stdev_a # standard deviation apriori

        self.residual = 0


class Quo:
    def __init__(self, origin, occupied, sighted, obs = 0.0, stdev_a = 0.0):
        self.origin = origin
        self.occupied = occupied
        self.sighted = sighted
        self.obs = obs
        self.stdev_a = stdev_a

        self.residual = 0

class Obs:

    def __init__(self, angs = [], dists = [], quos = [], basisAngs = []):
        self.angs = angs
        self.dists = dists
        self.quos = quos
        
        self.basisAngs = basisAngs

    def Add_ang(self,o):
        self.angs.append(o)

    def Add_dist(self,o):
        self.dists.append(o)

    def Add_quo(self,o):
        self.quos.append(o)

    def Add_basisAng(self,o):
        self.basisAngs.append(o)

    # ATTENTION: The LSMatrices don't uses this value
    def UpdateAngStdev_a (self):
        # ang
        for ba in self.basisAngs:
            for a in ba.angs:
                a.stdev_a = math.sqrt(2.0) * ba.stdev

    # ATTENTION: The LSMatrices don't uses this value
    def UpdateDistStdev_a (self):
        # dist
        for d in self.dists:
            d.stdev_a = math.sqrt(2)*(d.e_mm + d.e_ppm * 1e-6)
